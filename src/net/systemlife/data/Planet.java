package net.systemlife.data;

import java.util.Hashtable;

public class Planet {
	// ---------- // Données primaires
	private long id;
	private long tex;
	
	// ---------- // Props de la planète
	private Hashtable<String, Object> values = new Hashtable<>();
	private Hashtable<String, Boolean> edit = new Hashtable<>();
	
	// ---------- // Constructeur
	public Planet(long id, long tex) {
		this.id = id;
		this.tex = tex;
		
		setDefaultValues();
	}
	
	public void setDefaultValues() {
		values.clear();
		edit.clear();
		/*"name",
			"mass",
			"radius",
			"distmoy",
			"temp",
			"rotate",
			"atmos",
			"elemc",
			"elemh",
			"elemo",
			"elemn"*/
		
		values.put("name", "NoName");
		values.put("radius", 0d);
		values.put("distmoy", 0d);
		values.put("distmin", 0d);
		values.put("distmax", 0d);
		values.put("speed", 0d);
		values.put("temp", 0d);
		values.put("albedo", 30d);
		values.put("rotate", true);
		values.put("atmos", false);
		values.put("elemc", false);
		values.put("elemh", false);
		values.put("elemo", false);
		values.put("elemn", false);
		
		edit.put("name", true);
		edit.put("radius", true);
		edit.put("distmoy", true);
		edit.put("distmin", false);
		edit.put("distmax", false);
		edit.put("speed", false);
		edit.put("temp", false);
		edit.put("albedo", true);
		edit.put("rotate", true);
		edit.put("atmos", true);
		edit.put("elemc", true);
		edit.put("elemh", true);
		edit.put("elemo", true);
		edit.put("elemn", true);
	}
	
	// ---------- // Accesseurs primaires
	public long getId() { return id; }
	public long getTex() { return tex; }
	
	// ---------- // Accesseurs valeurs props
	public String[] getValueNameList() { return values.keySet().toArray(new String[values.keySet().size()]); }
	public Object getValue(String key) { return values.get(key); }
	public boolean getEdit(String key) { return edit.get(key); }
	
	// ---------- // Accesseurs edit props
	public void setValue(String key, Object value) { values.put(key, value); }
	public void setEdit(String key, boolean state) { edit.put(key, state); }
	
	// ---------- // Texte
	public String toString() {
		return (String) getValue("name");
	}	
}
