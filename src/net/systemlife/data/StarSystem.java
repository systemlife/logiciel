package net.systemlife.data;

import java.util.ArrayList;

public class StarSystem {
	// ---------- // Données
	private Sun sun = new Sun();
	private ArrayList<Planet> planets = new ArrayList<>();
	private long noNamePlanetCount = 1;
	private boolean edit = true;
	
	// ---------- // Accesseurs	
	public Sun getSun() { return sun; }
	
	public long getNoNamePlanetCount() { return noNamePlanetCount; }
	
	public Planet[] getPlanets() {
		return planets.toArray(new Planet[planets.size()]);
	}
	
	public Planet getPlanet(long id) {
		for(Planet planet : planets) {
			if(planet.getId() == id)
				return planet;
		}
		
		return null;
	}
	
	public boolean edit() { return edit; }
	
	// ---------- // Mutateurs	
	public void setNoNamePlanetCount(long arg) { noNamePlanetCount = arg; }
	
	public void setEdit(boolean state) { edit = state; }
	
	public void addPlanet(Planet planet) {
		planets.add(planet);
	}
	
	public void removePlanet(Planet planet) {
		planets.remove(planet);
	}
}
