package net.systemlife.data;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class StarSystemGenerator {	
	// ---------- // Generation
	public static long generateFreeId(StarSystem system) {
		long id = -1;
		Random rand = new Random(System.currentTimeMillis());
		
		do {
			id = rand.nextLong();			
		} while(system.getPlanet(id) != null);		
		
		return id;
	}
	
	public static StarSystem generateRandomSystem(StarSystem system) {
		Random rand = new Random(System.currentTimeMillis());
		
		int planetCount = (Math.abs(rand.nextInt()) % 4) + 2; // Nombre compris entre 2 et 7 inclus
		
		for(int i = 0; i < planetCount; i++) {
			try {
				system = generateRandomPlanet(system);
			} catch (TooManyPlanetsException e) {
				e.printStackTrace();
			}
		}
		
		return system;
	}
	
	public static StarSystem generateRandomPlanet(StarSystem system) throws TooManyPlanetsException {
		DataCenter.Instance().log("SystemGenerator", "Génération d'une planète");
		Random rand = new Random(System.currentTimeMillis());
		
		// Generation de l'ID
		if(system.getPlanets().length >= 10)
			throw new TooManyPlanetsException();
		
		long id = generateFreeId(system);
		
		// Generation de la distance
		double distRatio, dist;
		boolean distTooClose;
		
		do {
			distTooClose = false;
			
			distRatio = ((double) (Math.abs(rand.nextInt() * System.currentTimeMillis())) % 1e+8) / 1e+8; // Operation visant a rendre l'aléatoire plus précis
			dist = DataCenter.getPlanetMinDist() + (distRatio * (DataCenter.getPlanetMaxDist() - DataCenter.getPlanetMinDist())); // min + (ratio * (max - min))
			
			//System.out.println(dist);
			
			for(Planet planet : system.getPlanets()) {
				if((double) planet.getValue("distmoy") > dist - 1e+8 && (double) planet.getValue("distmoy") < dist + 1e+8) {
					distTooClose = true;
					//System.out.println("# " + (double) planet.getValue("distmoy"));
					break;
				}
			}			
		} while (distTooClose);
		
		// Generation du rayon
		double radiusRatio = ((double) (Math.abs(rand.nextInt() * System.currentTimeMillis())) % 10000) / 10000.0; // Operation visant a rendre l'aléatoire plus précis
		double radius = DataCenter.getPlanetMinRadius() + (radiusRatio * (DataCenter.getPlanetMaxRadius() - DataCenter.getPlanetMinRadius())); // min + (ratio * (max - min))
		radius = Math.round(radius);
		
		// Generiation de l'id de texture
		long tex = (Math.abs(rand.nextLong()) % 33) + 1;
		
		// Generation du nom
		long count = system.getNoNamePlanetCount();
		system.setNoNamePlanetCount(count + 1);	
		
		Planet planet = new Planet(id, tex);
		planet.setValue("name", "Planète " + count);
		planet.setValue("radius", radius);
		planet.setValue("distmoy", dist);
		planet.setValue("rotate", true);
		computePlanetDist(planet);
		computePlanetSpeed(planet);
		computePlanetTemp(planet);
		
		system.addPlanet(planet);
		
		DataCenter.Instance().log("SystemGenerator", "Generation de la planète réussie");
		
		return system;
	}
	
	// ---------- // Calculs
	public static void computePlanetSpeed(Planet planet) {
		double min = (double) planet.getValue("distmin");
		double max = (double) planet.getValue("distmax");
		
		// Calcul du demi-grand axe en ua
		double a = max / DataCenter.UA;
		
		// Calcul de la periode en années puis conversion en secondes
		double period = Math.sqrt(a * a * a);
		double periodSeconds = period * 365 * 24 * 60 * 60;
		
		// Calcul du perimètre de la trajectoire de la planete en km
		double perimeter = Math.PI * (3 * (min + max) - Math.sqrt((3 * min + max) * (min + 3 * max)));
		
		// Calcul de la vitesse en km / s
		double speed = perimeter / periodSeconds;
		
		// Mise a jour des données
		planet.setValue("speed", speed);		
	}
	
	public static void computePlanetDist(Planet planet) {
		// Calculs
		double distMoy = (double) planet.getValue("distmoy");
		double distMin = distMoy * 0.95f;
		double distMax = distMoy * 1.05f;
		
		// Mise a jour des données
		planet.setValue("distmin", distMin);
		planet.setValue("distmax", distMax);
	}
	
	public static void computePlanetTemp(Planet planet) {		
		// Source formule : http://www.astronomynotes.com/solarsys/s3c.htm
		
		double A = (double) planet.getValue("albedo") / 100.d;
		double D = (double) planet.getValue("distmoy") / DataCenter.UA;		
		
		double temp = (279 * Math.pow(1.d - A, 0.25d) * 1 / Math.sqrt(D)) - 273.15d;
		
		DataCenter.Instance().log("Temperature", "Temp : " + temp);
		
		planet.setValue("temp", temp);
	}
	
	// ---------- // Fichiers
	public static StarSystem loadStarSystemFromFile(File file) throws FileNotFoundException, IOException, ParseException, NullPointerException {
		StarSystem sys = new StarSystem();		
		JSONParser parser = new JSONParser();
		
		JSONObject root = (JSONObject) parser.parse(new FileReader(file));
		
		int noNameCount = (int) (long) root.get("nonamecount");
		sys.setNoNamePlanetCount(noNameCount);
		
		boolean edit = (boolean) root.get("edit");
		sys.setEdit(edit);
		
		JSONArray planetArray = (JSONArray) root.get("planets");
		for(Object obj : planetArray) {
			JSONObject obj2 = (JSONObject) obj;
			
			long id = (long) obj2.get("id");
			long tex = (long) obj2.get("tex");
			
			Planet planet = new Planet(id, tex);
			
			JSONArray datas = (JSONArray) obj2.get("data");
			for(Object data : datas) {
				JSONObject data2 = (JSONObject) data;
				String key = (String) data2.get("key");
				Object value = data2.get("value");
				
				planet.setValue(key, value);
				if(!edit)
					planet.setEdit(key, false);
			}
			
			sys.addPlanet(planet);
		}
			
		return sys;
	}
	
	@SuppressWarnings("unchecked")
	public static void saveStarSystemToFile(File file, StarSystem sys) throws IOException {
		JSONObject root = new JSONObject();
		root.put("nonamecount", sys.getNoNamePlanetCount());
		root.put("edit", sys.edit());
		
		JSONArray planets = new JSONArray();
		
		for(Planet planet : sys.getPlanets()) {
			JSONObject obj = new JSONObject();
			obj.put("id", planet.getId());
			obj.put("tex", planet.getTex());
			
			JSONArray datas = new JSONArray();
			String[] keys = planet.getValueNameList();
			
			for(String key : keys) {
				//DataCenter.Instance().log("Generator", key);
				JSONObject data = new JSONObject();
				data.put("key", key);
				data.put("value", planet.getValue(key));
				
				datas.add(data);
			}
			
			obj.put("data", datas);
			planets.add(obj);
		}
		
		root.put("planets", planets);
		
		FileWriter writer = new FileWriter(file);
		writer.write(root.toJSONString());
		writer.flush();
		writer.close();
	}
}
