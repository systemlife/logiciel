package net.systemlife.data;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import net.systemlife.gui.MainWindow;
import net.systemlife.scene.GraphicApp;

public class DataCenter {
	// ---------- // Version
	public static final int VERSION_MAJOR = 1;
	public static final int VERSION_MINOR = 1;
	public static final int VERSION_BUILD = 2;
	
	public static String getVersionText() {
		return VERSION_MAJOR + "." + VERSION_MINOR + "." + VERSION_BUILD;
	}
	
	public static String getCompleteName() {
		return "SystemLife " + getVersionText();
	}
	
	// ---------- // Singleton
	private static DataCenter _instance;
	public static DataCenter Instance() {
		if(_instance == null) {
			_instance = new DataCenter();
			_instance.init();
		}
		
		return _instance;
	}
	
	private DataCenter() {
		try {
			setupLog();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(-1);
		}
		
		log("DataCenter", "Lancement de " + DataCenter.getCompleteName());
	}
	
	// ---------- // Initialisation
	private void init() {
		mainWindow = new MainWindow();
	}
	
	// ---------- // Log
	private FileWriter logWriter;
	private void setupLog() throws IOException {
		File file = new File("log.txt");
		logWriter = new FileWriter(file, true);
	}
	
	public void log(String source, String text) {
		log("[" + source + "] " + text);
	}
	
	public void log(String text) {
		DateFormat format = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
		Date date = new Date();
		
		String message = "[" + format.format(date) + "] " + text;
		
		System.out.println(message);
		try {
			logWriter.write(message + "\n");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	// ---------- // Système solaire
	private StarSystem system = null;
	
	public StarSystem getStarSystem() { return system; }
	public void setStarSystem(StarSystem sys) { system = sys; }
	
	public void updateSystemRef() {
		mainWindow.updateGUI();
		graphicApp.triggerViewUpdate();
		mainWindow.fixGUI();
	}
	
	// ---------- // Données planètes min / max	
	public static double getPlanetMinRadius() { return 5e+3; };
	public static double getPlanetMaxRadius() { return 0.5e+5; };
	
	public static double getPlanetMinDist() { return 6e+7; };
	public static double getPlanetMaxDist() { return 6e+9; };
	
	// ---------- // Proportions données / VAR -> graphic
	public static final float PROP_SUN = 1000;
	public static final float PROP_PLANET_RADIUS = 100; // Attention : division
	public static final float PROP_PLANET_SPEED = 50;
	public static final float PROP_PLANET_DIST = 10000;
	public static final float PROP_PLANET_ZOOM_RATIO = 5;
	
	// ---------- // Interface graphique
	private MainWindow mainWindow;
	public MainWindow MainWindow() { return mainWindow; }
	
	public void updateFPSInTitle(int fps) {
		if(actualFile != null)
			mainWindow.setTitle(actualFile.getAbsolutePath() + " - " + getCompleteName() + " - " + fps + " FPS");
		else
			mainWindow.setTitle(getCompleteName() + " - " + fps + " FPS");
	}
	
	// ---------- // Application graphique
	private GraphicApp graphicApp;
	public GraphicApp GraphicApp() { return graphicApp; }
	public void setGraphicApp(GraphicApp app) { graphicApp = app; }
	
	// ---------- // Fichiers
	private boolean systemOpened = false;
	private File actualFile = null;
	private File lastDirectory = null;
	
	public boolean isSystemOpened() { return systemOpened; }
	public File getActualFile() { return actualFile; }
	public File getLastDirectory() { return lastDirectory; }
	
	public void setSystemOpened(boolean state) { systemOpened = state; }
	public void setActualFile(File file) { actualFile = file; }
	public void setLastDirectory(File file) { lastDirectory = file; }
	
	// ---------- // Fermeture
	public void close() {
		DataCenter.Instance().log("DataCenter", "Arret de " + DataCenter.getCompleteName());
		
		try {
			logWriter.write("####################\n");
			logWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(0);
	}
	
	// ---------- // Constantes mathématiques
	public static final float UA = 149567871; // 1 unité astronomique
	
}
