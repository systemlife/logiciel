package net.systemlife.data;

public final class Sun {
	// ---------- // Données
	private double mass = 1.989e+30;
	private double radius = 695500.0;
	private double temp = 5505.0;
	
	// ---------- // Accesseurs
	public double getMass() { return mass; }
	public double getRadius() { return radius; }
	public double getTemp() { return temp; }
	
	// ---------- // Texte
	public String toString() {
		return "Soleil";
	}
}
