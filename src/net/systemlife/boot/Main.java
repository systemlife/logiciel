package net.systemlife.boot;

import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import net.systemlife.data.DataCenter;

import javax.swing.*;
import javax.swing.UIManager.LookAndFeelInfo;

public class Main {

	public static void main(String[] args) {
		LwjglApplicationConfiguration.disableAudio = true;

		// Chargement de l'apparence Nimbus
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        } 
		    }
		} catch (Exception e) { // Si introuvable on met l'apparence Système
			try {
				UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			} catch (ClassNotFoundException | InstantiationException
					| IllegalAccessException | UnsupportedLookAndFeelException e1) {
				e1.printStackTrace();
			}
		}

		DataCenter.Instance().MainWindow().setVisible(true);
	}

}
