package net.systemlife.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import net.systemlife.data.DataCenter;
import net.systemlife.data.Planet;
import net.systemlife.data.StarSystem;
import net.systemlife.data.StarSystemGenerator;
import net.systemlife.data.Sun;
import net.systemlife.data.TooManyPlanetsException;

public class PropertiesPanel extends JPanel {
	private static final long serialVersionUID = -7137246412550066107L;

	// ---------- // Elements graphiques
	private JPanel panelTop = new JPanel();
		private JComboBox<Object> combo = new JComboBox<>(new Object[] {"Liste vide"});
		private JButton buttonCreate = new JButton("Créer");
		private JButton buttonRemove = new JButton("Supprimer");
	private JPanel panelProps = new JPanel();
		private JPanel panelPropsSun = new JPanel();
		private JPanel panelPropsPlanet = new JPanel();
		
	private long buttonCreateAntiSpamTime = System.currentTimeMillis();
	
	// ---------- // Reglages des champs de texte
	public static final int COLUMN_NUMBER = 15;
		
	private JComponent[] propsPlanetComponents;
	private String propsPlanetLabels[] = {
			"Nom :",
			//"Masse (kg) :",
			"Rayon (km) :",
			"Distance (millions de km) :",
			"Vitesse (km/s) :",
			"Température (°C) :",
			"Albedo (en %) :",
			"Rotation :",
			"Présence d'une atmosphère :",
			"Présence de carbone :",
			"Présence d'hydrogène :",
			"Présence d'oxygène :",
			"Présence d'azote :"};
	private String propsPlanetNames[] = {
			"name",
			//"mass",
			"radius",
			"distmoy",
			"speed",
			"temp",
			"albedo",
			"rotate",
			"atmos",
			"elemc",
			"elemh",
			"elemo",
			"elemn"
	};
	private Class<?> propsPlanetClass[] = {
			String.class,
			//Double.class,
			Double.class,
			Double.class,
			Double.class,
			Double.class,
			Double.class,
			Boolean.class,
			Boolean.class,
			Boolean.class,
			Boolean.class,
			Boolean.class,
			Boolean.class};
	
	// ---------- // Accesseurs
	public Object getSelectedItem() { return combo.getSelectedItem(); }
	
	// ---------- // Constructeur
	public PropertiesPanel() {
		setBorder(BorderFactory.createTitledBorder("Propriétés"));
		setLayout(new BorderLayout());
		
		setupTopPart();
		setupPropsPart();
	}
	
	public void setupTopPart() {
		//combo.setPreferredSize(new Dimension(200, 30));
		//buttonCreate.setPreferredSize(new Dimension(60, 30));	
		//buttonRemove.setPreferredSize(new Dimension(90, 30));
		
		combo.setPrototypeDisplayValue("XXXXXXXXXXXXXXXXXXXX");
		combo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Object item = combo.getSelectedItem();
				setSelectedObject(item);

				float ratio = DataCenter.Instance().MainWindow().viewControlPanel.getZoomRatio();
				
				if(item instanceof Sun) {
					DataCenter.Instance().GraphicApp().GraphicSystem().setZoom(ratio);
					DataCenter.Instance().GraphicApp().GraphicSystem().setTarget(-1);
					DataCenter.Instance().GraphicApp().GraphicSystem().updateTargetColor();
				} else if(item instanceof Planet) {
					DataCenter.Instance().GraphicApp().GraphicSystem().setZoom(ratio);
					DataCenter.Instance().GraphicApp().GraphicSystem().setTarget(((Planet) item).getId());
					DataCenter.Instance().GraphicApp().GraphicSystem().updateTargetColor();
				}
			}
		});
		
		buttonCreate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {				
				if(System.currentTimeMillis() - buttonCreateAntiSpamTime > 500) {
					buttonCreateAntiSpamTime = System.currentTimeMillis();
					
					StarSystem sys = DataCenter.Instance().getStarSystem();
					try {
						sys = StarSystemGenerator.generateRandomPlanet(sys);
						
						DataCenter.Instance().updateSystemRef();						
					} catch (TooManyPlanetsException e) {
						JOptionPane.showMessageDialog(DataCenter.Instance().MainWindow(), "Nombre limite de planètes atteinte", "Erreur", JOptionPane.ERROR_MESSAGE, (new ImageIcon("assets/icons/30/error.png")));
					}
				}
			}
		});
		
		buttonRemove.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(combo.getSelectedItem() instanceof Planet) {					
					Object[] options = {"Oui", "Non"};
					int choice = JOptionPane.showOptionDialog(DataCenter.Instance().MainWindow(),
							"Voulez-vous vraiment supprimer cette planète ?", "Supprimer une planète",
							JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
							new ImageIcon("assets/icons/30/warning.png"), options, options[1]);
					
					if(choice == 0) {
						DataCenter.Instance().getStarSystem().removePlanet((Planet) combo.getSelectedItem()); 
						DataCenter.Instance().updateSystemRef();
					}
				}
			}
		});
		
		panelTop.add(combo);
		panelTop.add(buttonCreate);
		panelTop.add(buttonRemove);
		
		add(panelTop, BorderLayout.NORTH);
		add(panelProps, BorderLayout.CENTER);
	}
	
	public void setupPropsPart() {		
		// Soleil
		{
			StarSystem sys = new StarSystem();
			Sun sun = sys.getSun();
			
			JTextField massField = new JTextField(Double.toString(sun.getMass()), COLUMN_NUMBER);
			massField.setEditable(false);
			massField.setBackground(Color.YELLOW);
			JTextField radiusField = new JTextField(Double.toString(sun.getRadius()), COLUMN_NUMBER);
			radiusField.setEditable(false);
			radiusField.setBackground(Color.YELLOW);
			JTextField tempField = new JTextField(Double.toString(sun.getTemp()), COLUMN_NUMBER);	
			tempField.setEditable(false);
			tempField.setBackground(Color.YELLOW);
			
			panelPropsSun.setLayout(new GridLayout(0, 2));
			panelPropsSun.add(new JLabel("Masse (kg) :"));
			panelPropsSun.add(massField);
			panelPropsSun.add(new JLabel("Radius (km) :"));
			panelPropsSun.add(radiusField);
			panelPropsSun.add(new JLabel("Température (°C) :"));
			panelPropsSun.add(tempField);
		}
		
		// Planètes
		{
			panelPropsPlanet.setLayout(new GridLayout(0, 2));
			
			propsPlanetComponents = new JComponent[propsPlanetNames.length];
			
			for(int i = 0; i < propsPlanetLabels.length; i++) {
				if(propsPlanetClass[i] == String.class) {
					propsPlanetComponents[i] = new JTextField("Value", COLUMN_NUMBER);
					((JTextField) propsPlanetComponents[i]).addActionListener(new TextFieldActionListener(propsPlanetNames[i], propsPlanetClass[i]));
				} else if(propsPlanetClass[i] == Double.class) {
					propsPlanetComponents[i] = new JTextField("Value", COLUMN_NUMBER);
					propsPlanetComponents[i].addKeyListener(new KeyAdapter() {
		                public void keyTyped(KeyEvent e) {
		                    char vChar = e.getKeyChar();
		                    if (!(Character.isDigit(vChar)
		                            || (vChar == KeyEvent.VK_BACK_SPACE)
		                            || (vChar == KeyEvent.VK_DELETE))) {
		                        e.consume();
		                    }
		                }
		            });
					
					((JTextField) propsPlanetComponents[i]).addActionListener(new TextFieldActionListener(propsPlanetNames[i], propsPlanetClass[i]));
				} else if(propsPlanetClass[i] == Boolean.class) {
					propsPlanetComponents[i] = new JCheckBox();
					((JCheckBox) propsPlanetComponents[i]).addActionListener(new CheckBoxActionListener(propsPlanetNames[i], (JCheckBox) propsPlanetComponents[i]));
				}
				
				panelPropsPlanet.add(new JLabel(propsPlanetLabels[i]));
				panelPropsPlanet.add(propsPlanetComponents[i]);
			}
		}
		
	}
	
	// ---------- // Mise a jour
	public void updateData() {
		combo.removeAllItems();

		if(DataCenter.Instance().isSystemOpened()) {
			StarSystem sys = DataCenter.Instance().getStarSystem();
			combo.addItem(sys.getSun());
			
			for(Planet planet : sys.getPlanets()) {
				combo.addItem(planet);
			}
			
			setUsable(true);
		} else {
			combo.addItem("Liste vide");
			setUsable(false);
		}
	}
	
	public void setSelectedObject(Object obj) {
		panelProps.removeAll();
		
		if(obj instanceof Sun) {
			panelProps.add(panelPropsSun);
		} else if(obj instanceof Planet) {
			Planet planet = (Planet) obj;
			
			NumberFormat nf = new DecimalFormat("0.###");
			
			for(int i = 0; i < propsPlanetNames.length; i++) {
				if(propsPlanetClass[i] == String.class) {
					String value = (String) planet.getValue(propsPlanetNames[i]);
					((JTextField) propsPlanetComponents[i]).setText(value);
					((JTextField) propsPlanetComponents[i]).setEditable(planet.getEdit(propsPlanetNames[i]));
					((JTextField) propsPlanetComponents[i]).setBackground(planet.getEdit(propsPlanetNames[i]) ? Color.WHITE : Color.YELLOW);
				} else if(propsPlanetClass[i] == Double.class) {
					double value = (double) planet.getValue(propsPlanetNames[i]);
					if(propsPlanetNames[i].equalsIgnoreCase("distmoy")) // Distance en million de km
						value /= 1e+6;
					((JTextField) propsPlanetComponents[i]).setText((nf.format(value)));
					((JTextField) propsPlanetComponents[i]).setEditable(planet.getEdit(propsPlanetNames[i]));
					((JTextField) propsPlanetComponents[i]).setBackground(planet.getEdit(propsPlanetNames[i]) ? Color.WHITE : Color.YELLOW);
				}
				else if(propsPlanetClass[i] == Boolean.class) {
					((JCheckBox) propsPlanetComponents[i]).setSelected((boolean) planet.getValue(propsPlanetNames[i])); 
					propsPlanetComponents[i].setEnabled(planet.getEdit(propsPlanetNames[i]));
				}
			}
			
			panelProps.add(panelPropsPlanet);
		}
		
		DataCenter.Instance().MainWindow().fixGUI(); // Reglage de l'affichage du a des ajouts de composants
	}
	
	// ---------- // Verouillage
	public void setUsable(boolean state) {
		combo.setEnabled(state);		
		
		boolean edit = false;
		
		if(DataCenter.Instance().getStarSystem() != null)
			edit = DataCenter.Instance().getStarSystem().edit();
		
		buttonCreate.setEnabled(state && edit);
		buttonRemove.setEnabled(state && edit);
	}
	
	// ========== //
	@SuppressWarnings("rawtypes")
	private class TextFieldActionListener implements ActionListener {
		private String name;
		private Class type;
	
		public TextFieldActionListener(String name, Class type) {
			this.name = name;
			this.type = type;
		}
		
		public void actionPerformed(ActionEvent e) {
			JTextField source = (JTextField) e.getSource();
			if(source.getBackground() == Color.RED)
				source.setBackground(Color.WHITE);
			
			Planet planet = (Planet) PropertiesPanel.this.getSelectedItem();
			NumberFormat nf = new DecimalFormat("0.###");
			
			if(type == String.class) {
				String value = source.getText();
				
				planet.setValue(name, value);
			} else if(type == Double.class) {
				double value = Double.parseDouble(source.getText());
				
				if(name.equalsIgnoreCase("distmoy")) {
					value *= 1e+6; // Distance en million de km
					if(value < DataCenter.getPlanetMinDist()) {
						value = DataCenter.getPlanetMinDist();
						source.setText(nf.format(value / 1e+6));
						source.setBackground(Color.RED);
					} else if(value > DataCenter.getPlanetMaxDist()) {
						value = DataCenter.getPlanetMaxDist();
						source.setText(nf.format(value / 1e+6));
						source.setBackground(Color.RED);
					}
				} else if(name.equalsIgnoreCase("radius")) {
					if(value < DataCenter.getPlanetMinRadius()) {
						value = DataCenter.getPlanetMinRadius();
						source.setText(nf.format(value));
						source.setBackground(Color.RED);
					} else if(value > DataCenter.getPlanetMaxRadius()) {
						value = DataCenter.getPlanetMaxRadius();
						source.setText(nf.format(value));
						source.setBackground(Color.RED);
					}
				} else if(name.equalsIgnoreCase("albedo")) {
					if(value < 0) {
						value = 0;
						source.setText(nf.format(value));
						source.setBackground(Color.RED);
					} else if(value > 100) {
						value = 100;
						source.setText(nf.format(value));
						source.setBackground(Color.RED);
					}
				} 
				
				planet.setValue(name, value);
				StarSystemGenerator.computePlanetDist(planet);
				StarSystemGenerator.computePlanetSpeed(planet);
				StarSystemGenerator.computePlanetTemp(planet);
				
				// Mise a jour de la vitesse et de la temp dans l'interface
				for(int i = 0; i < propsPlanetNames.length; i++) {
					if(propsPlanetNames[i].equalsIgnoreCase("speed")) {
						((JTextField) propsPlanetComponents[i]).setText(nf.format(planet.getValue("speed")));
					} else if(propsPlanetNames[i].equalsIgnoreCase("temp")) {
						((JTextField) propsPlanetComponents[i]).setText(nf.format(planet.getValue("temp")));
					}					
				}				
			}
			
			DataCenter.Instance().MainWindow().fixGUI();
			DataCenter.Instance().GraphicApp().triggerViewUpdate();
		}		
	}
	
	private class CheckBoxActionListener implements ActionListener {
		private String key;
		private JCheckBox item;
		
		public CheckBoxActionListener(String key, JCheckBox item) {
			this.key = key;
			this.item = item;
		}
		
		public void actionPerformed(ActionEvent e) {
			Planet planet = (Planet) PropertiesPanel.this.getSelectedItem();
			planet.setValue(key, item.isSelected());
			
			DataCenter.Instance().MainWindow().fixGUI();
			DataCenter.Instance().GraphicApp().triggerViewUpdate();
		}		
	}
}
