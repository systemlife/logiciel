package net.systemlife.gui;

import net.systemlife.data.DataCenter;

import javax.swing.*;
import java.awt.*;

/**
 * Created by Nanocryk on 17/02/14.
 */
public class AboutWindow extends JFrame {
    private final String text = "<html><b>Version du logiciel :</b><br />" +
            DataCenter.getCompleteName() + "<br />" +
            "<b>Développé par :</b><br />" +
            "Jérémy PICOT (Nanocryk)<br />" +
            "<br />" +
            "Ce logiciel a été réalisé dans le cadre du projet TPE de la classe de Première</html>";


    public AboutWindow() {
        setTitle("A propos");
        setIconImage((new ImageIcon("assets/icons/15/about.png")).getImage());
        setSize(400, 300);
        setResizable(false);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        JPanel mainPanel = new JPanel();
        //mainPanel.setBorder(BorderFactory.createTitledBorder("Informations"));

        JTextPane textPane = new JTextPane();
        textPane.setEditable(false);
        textPane.setContentType("text/html");
        textPane.setText(text);

        add(textPane);

        setVisible(true);
    }

    public static void main(String... args) {
        new AboutWindow();
    }
}
