package net.systemlife.gui;

import java.awt.BorderLayout;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.Box;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.systemlife.data.DataCenter;
import net.systemlife.scene.GraphicApp;

import com.badlogic.gdx.backends.lwjgl.LwjglAWTCanvas;

public class MainWindow extends JFrame {
	private static final long serialVersionUID = 4779333795384120348L;

	// ---------- // Panels
	private MainWindowMenuBar menuBar = new MainWindowMenuBar();
	
	private LwjglAWTCanvas canvas;
	private JPanel rightPanel = new JPanel();
	public final JProgressBar loadingBar = new JProgressBar();
	public final ViewControlPanel viewControlPanel = new ViewControlPanel();
	public final PropertiesPanel propertiesPanel = new PropertiesPanel();
	
	// ---------- // Constructeur
	public MainWindow() {
		DataCenter.Instance().log("MainWindow", "Création de la fenètre");
		
		setTitle(DataCenter.getCompleteName());
		setSize(800, 600);
		setLocationRelativeTo(null);
		setDefaultLookAndFeelDecorated(true);
		setIconImage((new ImageIcon("assets/icons/app.png")).getImage());
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		
		GraphicApp app = new GraphicApp();
		DataCenter.Instance().setGraphicApp(app);
		
		canvas = new LwjglAWTCanvas(app, false);
		add(canvas.getCanvas(), BorderLayout.CENTER);
		
		setupRightPanel();
		setupLoadingBar();
		
		addWindowListener(new WindowListener() {
			public void windowClosing(WindowEvent e) {
				MainWindow.this.userClose();
			}
			
			public void windowActivated(WindowEvent e) { }
			public void windowClosed(WindowEvent e) { }
			public void windowDeactivated(WindowEvent e) { }
			public void windowDeiconified(WindowEvent e) { }
			public void windowIconified(WindowEvent e) { }
			public void windowOpened(WindowEvent e) { }
		});
		
		setDefaultLookAndFeelDecorated(true);
	}
	
	// ---------- // Reglage des elements graphiques
	private void setupRightPanel() {
		viewControlPanel.setUsable(false);
		propertiesPanel.setUsable(false);
		
		rightPanel.setLayout(new BorderLayout());
		rightPanel.add(viewControlPanel, BorderLayout.NORTH);
		rightPanel.add(propertiesPanel, BorderLayout.CENTER);
		rightPanel.add(Box.createHorizontalStrut(400), BorderLayout.SOUTH);
	}
	
	private void setupLoadingBar() {
		loadingBar.setStringPainted(true);
		loadingBar.setString("Chargement des ressources");
		loadingBar.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				loadingBar.setString("Chargement des ressources : " + loadingBar.getValue() + "%");
				
				if(loadingBar.getValue() >= 100) {
					MainWindow.this.remove(loadingBar);
					MainWindow.this.add(rightPanel, BorderLayout.EAST);
					MainWindow.this.setJMenuBar(menuBar);
					
					//setExtendedState( getExtendedState() | JFrame.MAXIMIZED_BOTH );
					
					fixGUI(); // Reglage de l'affichage du a des ajouts de composants
					
					/*
					int width = MainWindow.this.getWidth();
					int height = MainWindow.this.getHeight();
					
					MainWindow.this.setSize(width + 1, height);
					MainWindow.this.setSize(width, height);*/
				}
			}
		});
		
		add(loadingBar, BorderLayout.SOUTH);
	}
	
	// ---------- // Mise a jour de l'interface
	public void updateGUI() {
		propertiesPanel.updateData();
		viewControlPanel.setUsable(DataCenter.Instance().isSystemOpened());
		menuBar.fileOpened(DataCenter.Instance().isSystemOpened());
	}
	
	// ---------- // Fermetures
	public void close() {		
		canvas.exit();
		dispose();
		DataCenter.Instance().close();
	}
	
	public void userClose() {
		String text = "Voulez-vous vraiment quitter ?";
		
		if(DataCenter.Instance().isSystemOpened())
			text += "<br /><i>Toutes les modifications non sauvegardées seront perdues</i>";
		
		Object[] options = {"Oui", "Non"};
		
		int choice = JOptionPane.showOptionDialog(MainWindow.this,
				"<html>" + text + "</html>", "Quitter",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE,
				new ImageIcon("assets/icons/30/question.png"), options, options[1]);
		
		if(choice == 0) {
			close();
		}
	}
	
	public boolean userFileClose() {
		if(DataCenter.Instance().isSystemOpened()) {
			DataCenter.Instance().log("MainWindow", "Demande de fermeture du fichier courant");
			
			Object[] options = {"Oui", "Non"};
			
			int choice = JOptionPane.showOptionDialog(MainWindow.this,
					"<html><i>Toutes modifications non sauvegardées seront perdues</i><br />"
					+ "Voulez-vous vraiment fermer ce système ?</html>", "Fermeture du système",
					JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE,
					new ImageIcon("assets/icons/30/warning.png"), options, options[1]);
			
			if(choice == 0) {
				DataCenter.Instance().setActualFile(null);
				DataCenter.Instance().setStarSystem(null);
				DataCenter.Instance().setSystemOpened(false);
				DataCenter.Instance().updateSystemRef();
				
				return true;
			}
			
			return false;
		}
		
		return true;
	}
	
	// ---------- // Fix de bug d'affichage
	public void fixGUI() {
		revalidate();
		repaint();
	}
}
