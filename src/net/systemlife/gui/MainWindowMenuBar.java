package net.systemlife.gui;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileFilter;

import org.json.simple.parser.ParseException;

import net.systemlife.data.DataCenter;
import net.systemlife.data.StarSystem;
import net.systemlife.data.StarSystemGenerator;

public class MainWindowMenuBar extends JMenuBar {
	private static final long serialVersionUID = -4491908826765794153L;
	
	// ---------- // Items du menu
	private JMenu menuFile = new JMenu("Fichier");
		private JMenu itemNew = new JMenu("Nouveau");
			private JMenuItem itemNewEmpty = new JMenuItem("Système vide");
			private JMenuItem itemNewRandom = new JMenuItem("Système aléatoire");
		private JMenuItem itemLoad = new JMenuItem("Ouvrir ...");
		private JMenuItem itemSave = new JMenuItem("Enregistrer");
		private JMenuItem itemSaveAs = new JMenuItem("Enregistrer sous ...");
		private JMenuItem itemClose = new JMenuItem("Fermer");
		private JMenuItem itemQuit = new JMenuItem("Quitter");
	
	private JMenu menuOptions = new JMenu("Options");
		private JMenuItem itemLock = new JMenuItem("Verrouiller le système ...");
		private JMenuItem itemAbout = new JMenuItem("A propos du logiciel");
		
	public MainWindowMenuBar() {
		
		this.setupFileMenu();	
		this.setupOptionsMenu();
		
		this.fileOpened(false);
	}
	
	private void setupFileMenu() {
		// Config du menu
		menuFile.setMnemonic(KeyEvent.VK_F);
		
		itemNew.setMnemonic(KeyEvent.VK_N);
		itemNew.setIcon(new ImageIcon("assets/icons/15/new.png"));
		
		itemNewEmpty.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK));
		itemNewEmpty.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(DataCenter.Instance().MainWindow().userFileClose()) {
					StarSystem sys = new StarSystem();
					DataCenter.Instance().setStarSystem(sys);
					DataCenter.Instance().setSystemOpened(true);
					DataCenter.Instance().updateSystemRef();
				}
			}
		});
		
		itemNewRandom.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		itemNewRandom.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(DataCenter.Instance().MainWindow().userFileClose()) {
					StarSystem sys = StarSystemGenerator.generateRandomSystem(new StarSystem());

					DataCenter.Instance().setStarSystem(sys);
					DataCenter.Instance().setSystemOpened(true);
					DataCenter.Instance().updateSystemRef();
				}
			}
		});
		
		itemNew.add(itemNewEmpty);
		itemNew.add(itemNewRandom);
		
		itemLoad.setMnemonic(KeyEvent.VK_O);
		itemLoad.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, ActionEvent.CTRL_MASK));
		itemLoad.setIcon(new ImageIcon("assets/icons/15/open.png"));
		itemLoad.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(DataCenter.Instance().MainWindow().userFileClose()) {					
					JFileChooser fileChooser = new JFileChooser();
					
					if(DataCenter.Instance().getLastDirectory() != null)
						fileChooser.setCurrentDirectory(DataCenter.Instance().getLastDirectory());
					
					fileChooser.setFileFilter(new SystemLifeFileFilter());
					if(fileChooser.showOpenDialog(DataCenter.Instance().MainWindow()) == JFileChooser.APPROVE_OPTION) {
					
						File file = fileChooser.getSelectedFile();
						DataCenter.Instance().setLastDirectory(file.getParentFile());
						
						StarSystem system = null;
						
						try {
							system = StarSystemGenerator.loadStarSystemFromFile(file);
							
							DataCenter.Instance().setStarSystem(system);
							DataCenter.Instance().setSystemOpened(true);
							DataCenter.Instance().setActualFile(file);
							DataCenter.Instance().updateSystemRef();
						} catch (FileNotFoundException e) {
							JOptionPane.showMessageDialog(DataCenter.Instance().MainWindow(), "Fichier introuvable", "Erreur", JOptionPane.ERROR_MESSAGE, (new ImageIcon("assets/icons/30/error.png")));
						} catch (IOException e) {
							JOptionPane.showMessageDialog(DataCenter.Instance().MainWindow(), "Erreur lors de la lecture du fichier", "Erreur", JOptionPane.ERROR_MESSAGE, (new ImageIcon("assets/icons/30/error.png")));
						} catch (ParseException | NullPointerException e) {
							JOptionPane.showMessageDialog(DataCenter.Instance().MainWindow(), "Erreur lors de la lecture du contenu du fichier", "Erreur", JOptionPane.ERROR_MESSAGE, (new ImageIcon("assets/icons/30/error.png")));
							e.printStackTrace();
						}
					}						
				}
			}
		});
	
		itemSave.setMnemonic(KeyEvent.VK_E);
		itemSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK));
		itemSave.setIcon(new ImageIcon("assets/icons/15/save.png"));
		itemSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(DataCenter.Instance().getActualFile() != null)
					save();
				else
					saveAs();
			}
		});
		
		itemSaveAs.setMnemonic(KeyEvent.VK_S);
		itemSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, ActionEvent.CTRL_MASK + ActionEvent.SHIFT_MASK));
		itemSaveAs.setIcon(new ImageIcon("assets/icons/15/saveas.png"));
		itemSaveAs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				saveAs();
			}
		});
		
		itemClose.setMnemonic(KeyEvent.VK_F);
		itemClose.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, ActionEvent.CTRL_MASK));
		itemClose.setIcon(new ImageIcon("assets/icons/15/close.png"));
		itemClose.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DataCenter.Instance().MainWindow().userFileClose();
			}
		});
	
		itemQuit.setMnemonic(KeyEvent.VK_Q);
		itemQuit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, ActionEvent.CTRL_MASK));
		itemQuit.setIcon(new ImageIcon("assets/icons/15/quit.png"));
		itemQuit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				DataCenter.Instance().MainWindow().userClose();
			}
		});
			
		// Ajouts
		menuFile.add(itemNew);
		menuFile.add(itemLoad);
		menuFile.addSeparator();
		menuFile.add(itemSave);
		menuFile.add(itemSaveAs);
		menuFile.add(itemClose);
		menuFile.addSeparator();
		menuFile.add(itemQuit);
		
		this.add(menuFile);
	}
	
	private void setupOptionsMenu() {
		menuFile.setMnemonic(KeyEvent.VK_A);
	
		itemLock.setMnemonic(KeyEvent.VK_L);
		itemLock.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, ActionEvent.CTRL_MASK));
		itemLock.setIcon(new ImageIcon("assets/icons/15/lock.png"));
		itemLock.addActionListener(new ActionListener() {
            @Override
			public void actionPerformed(ActionEvent arg0) {
				DataCenter.Instance().getStarSystem().setEdit(false);
				saveAs();
				DataCenter.Instance().getStarSystem().setEdit(true);
			}
		});
		/*
		itemAbout.setMnemonic(KeyEvent.VK_A);
		itemAbout.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_A, ActionEvent.CTRL_MASK));
		itemAbout.setIcon(new ImageIcon("assets/icons/15/about.png"));
        itemAbout.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                (new AboutWindow()).setLocationRelativeTo(DataCenter.Instance().MainWindow());
            }
        });*/
			
		menuOptions.add(itemLock);
		/*
        menuOptions.addSeparator();
		menuOptions.add(itemAbout);*/
		
		this.add(menuOptions);
	}
	
	public void fileOpened(boolean state) {
		boolean edit = false;
		
		if(DataCenter.Instance().getStarSystem() != null)
			edit = DataCenter.Instance().getStarSystem().edit();
		
		itemSave.setEnabled(state && edit);
		itemSaveAs.setEnabled(state && edit);
		itemLock.setEnabled(state && edit);
		itemClose.setEnabled(state);
	}
	
	private void saveAs() {
		JFileChooser fileChooser = new JFileChooser();
		
		if(DataCenter.Instance().getLastDirectory() != null)
			fileChooser.setCurrentDirectory(DataCenter.Instance().getLastDirectory());
		
		fileChooser.setFileFilter(new SystemLifeFileFilter());
		if(fileChooser.showSaveDialog(DataCenter.Instance().MainWindow()) == JFileChooser.APPROVE_OPTION) {
			File file = fileChooser.getSelectedFile();
			String filePath = file.getAbsolutePath();
			
			if(!filePath.endsWith(".slf"))
				file = new File(filePath + ".slf");
			
			try {
				StarSystemGenerator.saveStarSystemToFile(file, DataCenter.Instance().getStarSystem());
				JOptionPane.showMessageDialog(DataCenter.Instance().MainWindow(), "Le système a bien été sauvegardé", "Sauvegarde réussie", JOptionPane.INFORMATION_MESSAGE, (new ImageIcon("assets/icons/30/done.png")));
			} catch (IOException e) {
				JOptionPane.showMessageDialog(DataCenter.Instance().MainWindow(), "Erreur lors de l'écriture du fichier", "Erreur", JOptionPane.ERROR_MESSAGE, (new ImageIcon("assets/icons/30/error.png")));
			}
			
			DataCenter.Instance().setActualFile(file);
		}
	}
	
	private void save() {
		try {
			StarSystemGenerator.saveStarSystemToFile(DataCenter.Instance().getActualFile(), DataCenter.Instance().getStarSystem());
			JOptionPane.showMessageDialog(DataCenter.Instance().MainWindow(), "Le système a bien été sauvegardé", "Sauvegarde réussie", JOptionPane.INFORMATION_MESSAGE, (new ImageIcon("assets/icons/30/done.png")));
		} catch (IOException e) {
			JOptionPane.showMessageDialog(DataCenter.Instance().MainWindow(), "Erreur lors de l'écriture du fichier", "Erreur", JOptionPane.ERROR_MESSAGE, (new ImageIcon("assets/icons/30/error.png")));
		}
	}
	
	class SystemLifeFileFilter extends FileFilter {
		public boolean accept(File arg0) {
			if(arg0.isDirectory())
				return true;
			
			if(getExtension(arg0).equalsIgnoreCase("slf"))
				return true;
			return false;
		}
		
		public String getExtension(File f) {
	        String ext = "";
	        String s = f.getName();
	        int i = s.lastIndexOf('.');

	        if (i > 0 &&  i < s.length() - 1) {
	            ext = s.substring(i+1).toLowerCase();
	        }
	        return ext;
	    }

		public String getDescription() {
			return "Format de fichier SystemLife";
		}
	}
}
