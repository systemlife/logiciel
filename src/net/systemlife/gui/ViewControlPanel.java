package net.systemlife.gui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Hashtable;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import net.systemlife.data.DataCenter;
import net.systemlife.data.Planet;
import net.systemlife.data.Sun;

public class ViewControlPanel extends JPanel {
	private static final long serialVersionUID = -280483755329085194L;
	
	// ---------- // Elements graphiques
	private JSlider sliderAngle = new JSlider();
	private JSlider sliderZoom = new JSlider();
	private JSlider sliderSpeed = new JSlider();
	
	private JPanel targetPanel = new JPanel();
	private JButton targetOffButton = new JButton("Vue globale");
	private JButton targetOnButton = new JButton("Vue rapprochée");
	
	public float getZoomRatio() { return sliderZoom.getValue() / 100.f; }
	
	
	// ---------- // Constructeur
	public ViewControlPanel() {
		setLayout(new BoxLayout(this, BoxLayout.PAGE_AXIS));
		setBorder(BorderFactory.createTitledBorder("Affichage"));
		
		setupAngleSlider();
		setupZoomSlider();
		setupSpeedSlider();
		setupTargetButtons();
		
		add(sliderAngle);
		add(sliderZoom);
		add(sliderSpeed);
		add(targetPanel);
	}

	// ---------- // Construction des sliders
	private void setupAngleSlider() {
		sliderAngle.setMinimum(1);
		sliderAngle.setMaximum(179);
		sliderAngle.setValue(1);
		
		Hashtable<Integer, JLabel> sliderAngleLabels = new Hashtable<>();
		sliderAngleLabels.put(new Integer(1), new JLabel("Vue de dessus"));
		sliderAngleLabels.put(new Integer(90), new JLabel("Vue de côté"));
		sliderAngleLabels.put(new Integer(179), new JLabel("Vue de dessous"));
		
		sliderAngle.setLabelTable(sliderAngleLabels);
		sliderAngle.setPaintLabels(true);
		sliderAngle.setMinimumSize(new Dimension(200, 100));
		
		sliderAngle.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				float value = sliderAngle.getValue();
				DataCenter.Instance().GraphicApp().GraphicSystem().camera.setViewAngle(value);
			}
		});
	}
	
	private void setupZoomSlider() {
		sliderZoom.setMinimum(15);
		sliderZoom.setMaximum(80);
		sliderZoom.setValue(30);
		
		Hashtable<Integer, JLabel> sliderZoomLabels = new Hashtable<>();
		sliderZoomLabels.put(new Integer(15), new JLabel("Zoom -"));
		sliderZoomLabels.put(new Integer(80), new JLabel("Zoom +"));
		
		sliderZoom.setLabelTable(sliderZoomLabels);
		sliderZoom.setPaintLabels(true);
		sliderZoom.setMinimumSize(new Dimension(200, 100));
		
		sliderZoom.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				float ratio = getZoomRatio();
				
				Object item = DataCenter.Instance().MainWindow().propertiesPanel.getSelectedItem();
				
				if(item instanceof Sun) {
					DataCenter.Instance().GraphicApp().GraphicSystem().setZoom(ratio);
				} else if(item instanceof Planet) {
					DataCenter.Instance().GraphicApp().GraphicSystem().setZoom(ratio);
				}
			}
		});
	}
	
	private void setupSpeedSlider() {
		sliderSpeed.setMinimum(0);
		sliderSpeed.setMaximum(100);
		sliderSpeed.setValue(30);
		
		Hashtable<Integer, JLabel> sliderSpeedLabels = new Hashtable<>();
		sliderSpeedLabels.put(new Integer(0), new JLabel("Stop"));
		sliderSpeedLabels.put(new Integer(30), new JLabel("Moyen"));
		sliderSpeedLabels.put(new Integer(100), new JLabel("Rapide"));
		
		sliderSpeed.setLabelTable(sliderSpeedLabels);
		sliderSpeed.setPaintLabels(true);
		sliderSpeed.setMinimumSize(new Dimension(200, 100));
		
		sliderSpeed.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				int value = sliderSpeed.getValue();
				float speed = ((float) value) * 500.f;
				
				DataCenter.Instance().GraphicApp().GraphicSystem().setSpeed(speed);
			}
		});
	}
	
	// ---------- // Constructions des boutons de ciblage
	public void setupTargetButtons() {
		targetPanel.setLayout(new GridLayout(0, 2));
		
		targetOffButton.setBackground(Color.YELLOW);
		targetOnButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				targetOnButton.setBackground(Color.YELLOW);
				targetOffButton.setBackground(Color.WHITE);
				
				DataCenter.Instance().GraphicApp().GraphicSystem().setFollowTarget(true);
			}
		});
		
		targetOffButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				targetOffButton.setBackground(Color.YELLOW);
				targetOnButton.setBackground(Color.WHITE);
				
				DataCenter.Instance().GraphicApp().GraphicSystem().setFollowTarget(false);
			}
		});
		
		targetPanel.add(targetOffButton);
		targetPanel.add(targetOnButton);
	}
	
	// ---------- // Verouillage
	public void setUsable(boolean state) {
		sliderAngle.setEnabled(state);
		sliderZoom.setEnabled(state);
		sliderSpeed.setEnabled(state);
		targetOnButton.setEnabled(state);
		targetOffButton.setEnabled(state);
	}
}
