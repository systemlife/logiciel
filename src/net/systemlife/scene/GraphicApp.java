package net.systemlife.scene;

import net.systemlife.data.DataCenter;
import net.systemlife.data.Planet;
import net.systemlife.data.StarSystem;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;

public class GraphicApp implements ApplicationListener {
	// ---------- // Assets
	public final AssetManager assets = new AssetManager();
	private boolean assetsLoaded = false;
	
	// ---------- // Scene 3d
	private GraphicSystem system;
	public GraphicSystem GraphicSystem() { return system; }
	private boolean viewUpdate = false;
	
	// ---------- // Fonctions GDX
	public void create() {
		DataCenter.Instance().log("GraphicApp", "Chargement des ressources ...");
		
		assets.load("assets/bodies/star1.png", Texture.class);
		
		for(int i = 1; i <= 33; i++)
			assets.load("assets/bodies/planet" + i + ".png", Texture.class);
		
		system = new GraphicSystem();
		system.setZoom(0.2f);
	}

	public void dispose() {
		assets.clear();
	}

	public void render() {
		DataCenter.Instance().updateFPSInTitle(Gdx.graphics.getFramesPerSecond());
		
		Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		Gdx.gl.glClearColor(0.1f, 0.1f, 0.1f, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT | GL10.GL_DEPTH_BUFFER_BIT);
        
        if(!assetsLoaded) {
        	if(assets.update()) {
        		assetsLoaded = true;
        		
        		DataCenter.Instance().log("GraphicApp", "Chargement des ressources terminé");
        		DataCenter.Instance().MainWindow().loadingBar.setValue(100);
        	} else
        		DataCenter.Instance().MainWindow().loadingBar.setValue((int) (assets.getProgress() * 100));
        } else {
        	if(viewUpdate) {
        		updateView();
        		viewUpdate = false;
        	}
        	
        	system.update(Gdx.graphics.getDeltaTime());
        	system.render();
        }
	}

	public void resize(int arg0, int arg1) { }
	public void pause() { }
	public void resume() { }
	
	// ---------- // Mise a jour de la vue
	public void triggerViewUpdate() {
		viewUpdate = true;
	}
	
	private void updateView() {
		StarSystem sys = DataCenter.Instance().getStarSystem();
		
		system.clearBodies();
		
		if(!DataCenter.Instance().isSystemOpened())
			return;
		
		GraphicBody gSun = new GraphicBody(-1);
		gSun.setRadius((float) (sys.getSun().getRadius() / DataCenter.PROP_SUN));
		gSun.setTexture(assets.get("assets/bodies/star1.png", Texture.class));
		gSun.updateModel();
		system.addBody(gSun);
		
		//DataCenter.Instance().log("GraphicApp", "Nombre de planète: " + sys.getPlanets().length);
		
		for(Planet planet : sys.getPlanets()) {
			Texture tex;
			if(planet.getTex() >= 1 && planet.getTex() <= 33)
				tex = assets.get("assets/bodies/planet" + planet.getTex() + ".png", Texture.class);
			else
				tex = assets.get("assets/bodies/planet1.png", Texture.class);
		
			GraphicBody gBody = new GraphicBody(planet.getId());
			gBody.setDistData((float) (double) planet.getValue("distmin") / DataCenter.PROP_PLANET_DIST, (float) (double) planet.getValue("distmax") / DataCenter.PROP_PLANET_DIST, (float) (double) planet.getValue("speed") / DataCenter.PROP_PLANET_SPEED); 
			gBody.setRadius((float) (double) planet.getValue("radius") / DataCenter.PROP_PLANET_RADIUS);
			
			boolean rotate = (boolean) planet.getValue("rotate");
			
			if(rotate)
				gBody.setRotData((float) Math.toRadians(27), 1.f); 
			
			gBody.setTexture(tex);
			gBody.updateModel();
			
			system.addBody(gBody);
		}
		
		system.updateTargetColor();
	}
}
