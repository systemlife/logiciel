package net.systemlife.scene;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.VertexAttributes.Usage;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute;
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;

public class GraphicBody {
	// ---------- // Id du coprs (-1 pour le soleil)
	private long id;
	public long getId() { return id; }
	
	// ---------- // Model 3d
	private Model model;
	private ModelInstance instance;
	private Model model2;
	private ModelInstance instance2;
	private Texture texture;
	
	public void setTexture(Texture tex) { texture = tex; }
	
	// ---------- // Constructeur
	public GraphicBody(long id) {
		this.id = id;
	}
	
	// ---------- // Destruction
	public void destroy() {
		if(model != null)
			model.dispose();
		if(model2 != null)
			model2.dispose();
	}
	
	// ---------- // Données du model
	private float radius = 0, min = 0, max = 0;
	private float posSpeed;
	private float rotSpeed = 0, tilt = 0;
	private float posAngle, posX, posZ;
	private float rotAngle;
	
	public float getRadius() { return radius; }
	
	public float getPosX() { return posX; }
	public float getPosZ() { return posZ; } 
	
	public float getDistMoy() {
		return (min + max) / 2;
	}
	
	public void setDistData(float min, float max, float speed) {
		this.min = min;
		this.max = max;
		
		float perimeter = (float) (Math.PI * (3 * (min + max) - Math.sqrt((3 * min + max) * (min + 3 * max))));
		posSpeed = (perimeter != 0) ? (float) (speed / perimeter * 2 * Math.PI) : 0.f;		
	}
	
	public void setRotData(float tilt, float speed) {
		this.tilt = tilt;
		this.rotSpeed = speed;
	}
	
	public void setRadius(float radius) { this.radius = radius; }
	
	// --------- // Mise a jour du model
	public void updateModel() {
		destroy();
		
		ModelBuilder build = new ModelBuilder();
		float d = radius * 2;
		model = build.createSphere(d, d, d, 150, 150, new Material(),
				Usage.Position | Usage.Normal | Usage.TextureCoordinates);
		
		instance = new ModelInstance(model);
		instance.materials.first().set(TextureAttribute.createDiffuse(texture));
		
		Material material = new Material(
				            new BlendingAttribute(1.f),
				            new FloatAttribute(FloatAttribute.AlphaTest, 0.5f));
		
		build.begin();
		MeshPartBuilder bPartBuilder = build.part("ellipse", GL10.GL_LINES, Usage.Position | Usage.Normal, material);
	      bPartBuilder.ellipse(min * 2, max * 2, min * 2, max * 2, 500, new Vector3(0, 0, 0), new Vector3(0, 1, 0));
		model2 = build.end();
		instance2 = new ModelInstance(model2);
		setEllipseColor(new Color(DEFAULT_COLOR));
	}
	
	// ---------- // Couleur de l'ellipse
	public static final Color DEFAULT_COLOR = new Color(0.2f, 0.2f, 0.2f, 1f);
	public static final Color SELECTED_COLOR = new Color(0.5f, 0.5f, 0f, 1f);
	
	public void setEllipseColor(Color color) {
		instance2.materials.get(0).set(new ColorAttribute(ColorAttribute.Diffuse, color));
	}
	
	// ---------- // Mise a jour cyclique
	public void update(float time) {
		posAngle = time * posSpeed;
		rotAngle = (time * rotSpeed) / 50.f;
		
		posX = (float) (min * Math.cos(posAngle));
		posZ = (float) (max * Math.sin(posAngle));
		
		//DataCenter.Instance().log("GraphicBody", id + " : " + posX + " ; " + posZ);
	}
	
	// --------- // Rendu
	public void render(ModelBatch batch, Environment envi, float scale) {
		instance.transform.idt();
		instance.transform.setToScaling(scale, scale, scale);
		instance.transform.translate((float) posX, 0, (float) posZ);
		instance.transform.rotate(new Vector3(0.f, 0.f, 1.f), (float) tilt);
		instance.transform.rotate(new Vector3(0.f, 1.f, 0.f), (float) rotAngle);
		
		instance2.transform.idt();
		instance2.transform.setToScaling(scale, scale, scale);
		
		if(id == -1) // Si soleil
			batch.render(instance);
		else
			batch.render(instance, envi);
		
		batch.render(instance2);
	}
}
