package net.systemlife.scene;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.PointLight;
import com.badlogic.gdx.math.Vector3;

public class GraphicSystem {
	// ---------- // Objets 3D
	private Environment envi = new Environment();
	private ModelBatch batch;
	private ArrayList<GraphicBody> bodies = new ArrayList<GraphicBody>();
	
	public final CameraAdapter camera;
	
	public void addBody(GraphicBody body) {	bodies.add(body); }
	public void clearBodies() {
		// Destruction des elements
		for(GraphicBody body : bodies)
			body.destroy();
		
		bodies.clear();
	}
	public GraphicBody[] getBodyArray() { return bodies.toArray(new GraphicBody[bodies.size()]); }
	public GraphicBody getBodyById(long id) {
		for(GraphicBody body : bodies) {
			if(body.getId() == id)
				return body;
		}
		
		return null;
	}
	
	// ----------- // Paramètres scène 3d
	private float time = 1e+8f;
	private float zoom = 1.f; 
	private float scale = 1;
	public static float SCALE_FACTOR = 1f;
	private float speed = 15000.f;
	private long target = -1;
	private boolean followTarget = false;
	
	public void setTarget(long id) {
		target = id;
	}
	
	public void setZoom(float zoom) { this.zoom = zoom; }
	public void setTime(float time) { this.time = time; }
	// public void setScale(float scale) { this.scale = scale; }
	public void setSpeed(float speed) { this.speed = speed; }
	public void setFollowTarget(boolean state) { followTarget = state; }
	
	// ---------- // Constructeur
	public GraphicSystem() {
		envi.clear();
		envi.add(new PointLight().set(Color.WHITE, 0, 0, 0, 100000000));
		envi.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.8f, 0.8f, 0.8f, 0.8f));
		batch = new ModelBatch();
		camera = new CameraAdapter();
	}
	
	// ---------- // Mise a jour cyclique
	public void update(float delta) {
		if(Gdx.input.isKeyPressed(Keys.F5))
			followTarget = true;
		else if(Gdx.input.isKeyPressed(Keys.F6))
			followTarget = false;
		
		//time += speed * delta;
		
		for(GraphicBody body : bodies)
			body.update(time);
		
		GraphicBody body = getBodyById(target);
		
		if(bodies.size() == 0) {
			time += speed * delta;
			scale = 1.f;
			camera.setTarget(new Vector3(0, 0, 0));
		} else if(!followTarget) { // ZOOM SOLEIL
			time += speed * delta;
			scale = (zoom * zoom) / 15.f;
			camera.setTarget(new Vector3(0, 0, 0));
		} else {
			if(body != null && body.getId() != -1) {
				time += speed * delta * (body.getDistMoy() / 10000.f) ;
				scale = zoom / 5.f;
				camera.setTarget(new Vector3(body.getPosX() * scale * SCALE_FACTOR, 0, body.getPosZ() * scale * SCALE_FACTOR));
			} else { // ZOOM SOLEIL
				time += speed * delta;
				scale = (zoom * zoom) / 15.f;
				camera.setTarget(new Vector3(0, 0, 0));
			}
		}
		
		camera.update();
	}
	
	// ---------- // Mise a jour de la couleur
	public void updateTargetColor() {
		for(GraphicBody body : bodies) {
			if(body.getId() == target)
				body.setEllipseColor(GraphicBody.SELECTED_COLOR);
			else
				body.setEllipseColor(GraphicBody.DEFAULT_COLOR);
		}
	}
	
	// ---------- // Rendu
	public void render() {		
		camera.update();
		
		batch.begin(camera.getCamera());
		
		for(GraphicBody body : bodies) {
			body.render(batch, envi, scale * SCALE_FACTOR);
		}
		
		batch.end();
	}
	
}
