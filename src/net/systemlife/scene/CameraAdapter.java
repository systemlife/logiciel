package net.systemlife.scene;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.math.Vector3;

public class CameraAdapter {
	// ---------- // Camera
	private PerspectiveCamera camera;
	public PerspectiveCamera getCamera() { return camera; }
	
	// ---------- // Position
	private Vector3 target = new Vector3();
	private float dist = 1500;
	private Vector3 pos = new Vector3();
	
	public void setTarget(Vector3 target) { this.target = target; }
	public void setViewAngle(float angle) {
		angle = (float) Math.toRadians(angle);
		pos = new Vector3(0, 0, 0);
		pos.y = (float) (dist * Math.cos(angle));
		pos.x = (float) (dist * Math.sin(angle));
	}
	
	// ---------- // Constructeur
	public CameraAdapter() {
		camera = new PerspectiveCamera(50, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		camera.near = 0.1f;
		camera.far = 1000000.f;
		setViewAngle(1);
	}
	// ---------- // Mise a jour
	public void update() {
		//DataCenter.Instance().log("Camera", "target: " + target + " ; pos: " + pos);
		
		Vector3 nPos = new Vector3(target.x + pos.x, target.y + pos.y, target.z + pos.z);
		
		camera.viewportWidth = Gdx.graphics.getWidth();
		camera.viewportHeight = Gdx.graphics.getHeight();
		camera.position.set(nPos);
		camera.lookAt(target);
		camera.update();
	}
}
